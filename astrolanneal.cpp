/* -*- C++ -*-
 *
 * --------------------------------------------------------------------------
 * astrolanneal.cpp, Version 2023/10/19
 *
 * 2023- Daniel Gruen <daniel.gruen@lmu.de>
 * --------------------------------------------------------------------------
 *
 */

#include <iostream>
#include <iomanip>
#include <cmath>
#include <cstdlib>
#include <cstring>
#include <fstream>
#include <sstream>
#include <vector>
#include <algorithm>
#include <cassert>

using namespace std;

// annealing parameters
const double thot = 30000.;
const double tfactor = 0.999997;
const int freeze_timeout = 500000;
// slow down cooling in this temperature range by a factor
const double tslowcoolmax = 1500.; // this is used as the starting temperature when reading in a draw from file
const double tslowcoolmin = 0.;
const double tslowdownfactor = 10.;

bool talk=true;

// settings for a possible lab schedule
int ideal_npartners; // this is how many differnent people you should meet in a semester
int nrooms; // number of rooms, setting the maximum number of parallel experiments
int nexperiments; // number of different experiments
int nslots; // number of subsequent slots for experiments

// how bad are things from an advisor side
const double advisor_impossible_experiment  = 1000.; // advising an experiment the advisor did not list as possible
const double advisor_unpreferred_experiment =  200.; // advising an experiment the advisor did not list as preferred
const double advisor_double_booking = 2000.;         // advising two experiments in the same week
const double advisor_notime = 3000;                  // advising when the advisor said they have no time
const double advisor_wrongsws = 400.;                // advising the wrong number of experiments
const double advisor_multipleexperiments = 200.;     // advisor is teaching multiple different experiments

// how bad are things from a student side
const double student_impossible_experiment  = 2000.; // doing an experiment the student did not list as possible
const double student_unpreferred_experiment =   50.; // doing an experiment the student did not list as preferred
const double student_double_booking = 3000.;         // doing two experiments in the same week
const double student_double_experiment = 3000.;      // doing the same experiment twice
const double student_wrong_npartner = 100.;          // working with a non-optimal number of different partners
const double student_nopartner = 200.;               // doing an experiment on their own
const double student_twopartners = 300.;             // working in a group of three
const double student_disliked_partner = 500.;        // working with a partner they actively did not want to work with
const double student_random_partner = 150.;            // working with a partner they had no preference on

// from a timeslot side
const double rooms_overflow = 500.;                  // more parallel experiments than we have rooms
const double experiments_overflow = 2000.;           // the same experiment is done more times than possible in parallel
const double experiment_tooearly = 1500;             // experiment happening in an earlier slot than can be accommodated

// global variables
int nstudents;
int nadvisors;
int nexperiments_total; // number of advisor-[group of students]-experiment-slot tuples
int extra_duration;     // number of advisor-[group of students]-experiment-slot tuples accumulated due to experiments with duration>1
struct experiment;      // advisor-[group of students]-experiment tuple
vector<experiment> experiments; // list of advisor-[group of students]-experiment tuples
struct student;         // student
struct advisor;         // advisor


// experiment properties are stored as vectors
vector<string> experiment_names;       // descriptive name
vector<int> experiments_simultaneous;  // how many times the experiment can run in parallel
vector<int> experiments_duration;      // how many slots the experiment takes
vector<int> experiments_earliestslot;

advisor *advisors=0;
student *students=0;

struct experiment {
  advisor *a;
  student *s[3];
  int nstudents;
  int time; // when it starts; duration is in experiments_duration[experiment_id]
  int experiment_id;
  vector<int> notime; // slots in which this experiment should not happen
};

struct advisor {
  int id;
  string name;
  vector<int> notime;
  vector<int> preferred; // list of preferred experiments of this advisor
  vector<int> possible;  // list of possible experiments for this advisor
  int nexperiments;
};

struct student {
  int id;
  string name;
  int nexperiments;
  vector<int> preferred; // list of preferred experiments of this student
  vector<int> possible;  // list of possible experiments for this student
  vector<int> like; // ids of students they would like to work with
  vector<int> dislike; // ids of students they would NOT like to work with
};


int max_duration = 1; // longest duration of any experiment

void readFiles(char *experimentsFile, char *advisorFile, char *studentFile, char *initialFile=0, int frozen_time=0)
// function to read in the configuration files
// and to initialize schedule (either from FITS file or randomly)
{
  ifstream ein(experimentsFile);
  ifstream ain(advisorFile);
  ifstream sin(studentFile);

  string buf;

  getline(ein, buf); // header
  ein >> nexperiments >> nslots >> nrooms >> ideal_npartners;
  for (int i=0; i<nexperiments; i++) {
    int ibuf;
    ein >> ibuf;
    if (ibuf != i) {
      cout << "error; you didn't properly order experiments as 0..nexperiments-1!" << endl;
      exit(1);
    }
    ein >> buf;
    experiment_names.push_back(buf);
    ein >> ibuf; assert(ibuf>0);
    experiments_simultaneous.push_back(ibuf);
    ein >> ibuf; assert(ibuf>0);
    experiments_duration.push_back(ibuf);
    ein >> ibuf;
    experiments_earliestslot.push_back(ibuf);
  }
  ein.close();

  // get some numbers first
  nadvisors=nstudents=0;

  getline(sin, buf); // header

  for ( std::string line; getline( sin, line ); ) {
    nstudents++;
  }
  sin.close();

  getline(ain, buf); // header

  for ( std::string line; getline( ain, line ); ) {
    nadvisors++;
  }
  ain.close();

  advisors = new advisor[nadvisors];
  students = new student[nstudents];

  if (talk) cout << "reading " << nstudents << " students and " << nadvisors << " advisors" << endl;

  sin.open(studentFile);

  getline(sin, buf); // header

  int nexperiments_students=0;

  for (int i=0; i<nstudents; i++) {
    string line;
    getline( sin, line );
    stringstream ssl(line);
    ssl >> students[i].id >> students[i].name >> students[i].nexperiments;
    nexperiments_students += students[i].nexperiments;

    if (talk) cout << "Student " << students[i].name << " want to do " << students[i].nexperiments << " experiments" << endl;

    // ID, name, number of experiments, preferred experiment IDs, -, other possible experiment IDs
    if (students[i].id != i) {
      cout << "error; you didn't properly order students as 0..nstudents-1!" << endl;
      exit(1);
    }

    ssl >> buf;
    //cout << "read1 " << buf << endl;
    while((buf != "-") && ssl) {
      students[i].preferred.push_back(stoi(buf));
      if (talk) cout << "Student " << students[i].name << " would like to do experiment " << experiment_names[stoi(buf)] << endl;
      ssl >> buf;
      //cout << "read2 " << buf << endl;
    }
    if (ssl) ssl >> buf;
    //cout << "read3 " << buf << endl;
    students[i].possible = students[i].preferred;
    while((buf != "-") && ssl) {
      students[i].possible.push_back(stoi(buf));
      if (talk) cout << "Student " << students[i].name << " could do experiment " << experiment_names[stoi(buf)] << endl;
      ssl >> buf;
      //cout << "read " << buf << endl;
    }
    if (ssl) ssl >> buf;
    while((buf != "-") && ssl) {
      students[i].like.push_back(stoi(buf));
      if (talk) cout << "Student " << students[i].name << " would like to work with student " << stoi(buf) << endl;
      ssl >> buf;
      //cout << "read " << buf << endl;
    }
    if (ssl) ssl >> buf;
    while((buf != "-") && ssl) {
      students[i].dislike.push_back(stoi(buf));
      if (talk) cout << "Student " << students[i].name << " would NOT like to work with student " << stoi(buf) << endl;
      ssl >> buf;
      //cout << "read " << buf << endl;
    }

    assert(students[i].possible.size()>=students[i].nexperiments);
  }
  sin.close();

  ain.open(advisorFile);
  getline(ain, buf); // header

  nexperiments_total=0;

  int j=0;
  for (int i=0; i<nadvisors; i++) {
    string line;
    getline( ain, line );
    //cout << "read " << line << endl;
    stringstream ssl(line);
    // ID, name, number of experiments, preferred experiment IDs, -, other possible experiment IDs, -, integer dates that do not work
    ssl >> advisors[i].id >> advisors[i].name >> advisors[i].nexperiments;
    nexperiments_total += advisors[i].nexperiments;

    if (talk) cout << "Advisor " <<  advisors[i].name << " would like to teach " <<  advisors[i].nexperiments << " experiments" << endl;

    if (advisors[i].id != i) {
      cout << "error; you don't properly order advisors as 0..nadvisors-1!" << endl;
      exit(1);
    }

    ssl >> buf;
    while((buf != "-") && ssl) {
      advisors[i].preferred.push_back(stoi(buf));
      if (talk) cout << "Advisor " << advisors[i].name << " would like to teach experiment " << experiment_names[stoi(buf)] << endl;
      ssl >> buf;
    }
    advisors[i].possible = advisors[i].preferred;
    ssl >> buf;
    while((buf != "-") && ssl) {
      advisors[i].preferred.push_back(stoi(buf));
      if (talk) cout << "Advisor " <<  advisors[i].name << " could also teach experiment " << experiment_names[stoi(buf)] << endl;
      ssl >> buf;
    }
    ssl >> buf;
    while( ssl ) {
      advisors[i].notime.push_back(stoi(buf));
      if (talk) cout << "Advisor " <<  advisors[i].name << " does not have time in slot " << buf << endl;
      ssl >> buf;
    }
  }

  if(talk) {
  cout << "In total we can offer " << nexperiments_total
       << " experiments according to advisor availability." << endl;
  cout << "Given the total of " << nexperiments_students
       << " experiments requested by students, this means an average group size of "
       << double(nexperiments_students)/nexperiments_total << endl;
	}

  if (nexperiments_students/nexperiments_total>3) {
    nexperiments_total = int(nexperiments_students/3+0.999);
    if(talk) cout << "Because that number is too large, we will need additional advising and will schedule "
         << nexperiments_total << " experiments." << endl;
  }

int ninitial = 0;
vector<int> nadvising; nadvising.resize(nadvisors, 0); // how many experiments this advisor is advising in the initial plan
vector<int> ntaking; ntaking.resize(nstudents, 0); // how many experiments this student is taking in the initial plan

extra_duration = 0; // extra slots accumulated by experiments with duration > 1

if(initialFile) {
	if(talk) cout << "Reading in previous schedule." << endl;
  ifstream iin(initialFile);
	getline(iin, buf); // header
	for( std::string line; getline( iin, line ); )
  {
       ninitial++;
  }
  iin.close();

  iin.open(initialFile);
	getline(iin, buf); // header
  if(talk) cout << "Initializing to " << ninitial << " experiments." << endl;
	experiments.resize(ninitial);

	for(int i=0; i<ninitial; i++)
	{
	 string line;
	 getline( iin, line );
	 stringstream ssl(line);
	 int s[3]; int a;
	 ssl >> a >> s[0] >> s[1] >> s[2] >> experiments[i].nstudents >> experiments[i].time >> experiments[i].experiment_id;

   if(a<0 || a>=nadvisors) {
		 cerr << "Advisor ID " << s[j] << " is out of bounds for " << nadvisors << " advisors. Exiting." << endl;
		 exit(1);
	 }

	 experiments[i].a = &(advisors[a]);
	 nadvising[a] += experiments_duration[experiments[i].experiment_id]; // counts double for double-slot experiments

   extra_duration += (experiments_duration[experiments[i].experiment_id]-1);

	 for(int j=0; j<experiments[i].nstudents; j++) {
		 if(s[j]<0 || s[j]>=nstudents) {
			 cerr << "Student ID " << s[j] << " is out of bounds for " << nstudents << " students. Exiting." << endl;
			 exit(1);
		 }
		 experiments[i].s[j] = &(students[s[j]]);
		 ntaking[s[j]] += experiments_duration[experiments[i].experiment_id]; // counts double for double-slot experiments
	 }
 }
}

if(ninitial >= nexperiments_total) {
  if(talk) cout << "The previous schedule contains a larger or equal number of experiments than we would ideally schedule, and we are keeping it that way." << endl;
} else { // we are missing experiments!
	experiments.resize(nexperiments_total); // we may not need this many in the list if duration>1 - but that may change later if we remove a two-slot experiment and replace it by two one-slot experiments!
}

for(int i=ninitial; i<nexperiments_total; i++) {
  experiments[i].a=0;
	experiments[i].s[0] = experiments[i].s[1] = experiments[i].s[2] = 0;
	experiments[i].nstudents = 0;
	experiments[i].time = -1;
	experiments[i].experiment_id=-1;
}


// initialize experiment schedule - start with how many advisors say they want to advise
j=ninitial;

for(int i=0; i<nadvisors; i++) // fill experiments as offered by advisors
{
	// time offset for advisor
	int time_offset=0;
	if(nslots>advisors[i].nexperiments)
		time_offset = rand()%(nslots-advisors[i].nexperiments+1);

  for(int k=0; k<advisors[i].nexperiments-nadvising[i]; k++) // k is the number of slots past time_offet we are
	{
		experiments[j].a = &(advisors[i]);
		experiments[j].s[0] = experiments[j].s[1] = experiments[j].s[2] = 0;
		experiments[j].nstudents = 0;
		experiments[j].time = time_offset+k;
		if(advisors[i].preferred.size()>0)
			experiments[j].experiment_id = advisors[i].preferred[rand()%advisors[i].preferred.size()];
		else
		  experiments[j].experiment_id = advisors[i].possible[rand()%advisors[i].possible.size()];

      if (experiments[j].time+experiments_duration[experiments[j].experiment_id]>nslots)
        experiments[j].time = nslots-experiments_duration[experiments[j].experiment_id]; // avoid running beyond the schedule

      k += (experiments_duration[experiments[j].experiment_id]-1); // counts up extra k if duration of experiment is > 1
      extra_duration += (experiments_duration[experiments[j].experiment_id]-1);
      // cout << "I've filled experiment number " << j << " with experiment ID "
      //      << experiments[j].experiment_id << " advised by advisor ID " << experiments[j].a->id << endl;
      j++;
    }
  }

  // potentially create new experiments if we need more than advisors say they want to advise
  for (; j<nexperiments_total-extra_duration; j++) {
    int i = rand() % nadvisors;
    if(talk) cout << "creating an additional experiment for advisor " << i << endl;

    experiments[j].a = &(advisors[i]);
    experiments[j].s[0] = experiments[j].s[1] = experiments[j].s[2] = 0;
    experiments[j].nstudents = 0;
    experiments[j].time = rand()%(nslots-max_duration+1); // never exceeds timeframe this way
    if (advisors[i].preferred.size()>0)
      experiments[j].experiment_id = advisors[i].preferred[rand()%advisors[i].preferred.size()];
    else
      experiments[j].experiment_id = advisors[i].possible[rand()%advisors[i].possible.size()];

		if(experiments[j].time<frozen_time) experiments[j].time=frozen_time; // avoid scheduling a new experiment in the past

    extra_duration += (experiments_duration[experiments[j].experiment_id]-1);
  }


  // filling experiments with students in a greedy algorithm
  for (int j=0; j<nstudents; j++) {
		if(talk) cout << "Initially assigning student " << j << " to experiments" << endl;

    int extra_duration_student=0; // the extra-duration experiment slots the student has accumulated

    random_shuffle(students[j].preferred.begin(), students[j].preferred.end());
    random_shuffle(students[j].possible.begin()+students[j].preferred.size(), students[j].possible.end());
    random_shuffle(experiments.begin(), experiments.end()-extra_duration);

    // assert(students[j].possible.size() >= students[j].nexperiments); // no longer necessarily true given duration>1 experiments

   for(int i=0; i<ntaking[j]-students[j].nexperiments; i++) {
		 if(talk) cout << "Student " << j << " is taking more experiments than required in the initial plan. Removing them from one experiment." << endl;
     for(int k=0; k<nexperiments_total-extra_duration; k++) {
			 if(experiments[k].time<frozen_time) continue; // do not change a past experiment
			 for(int l=0; l<experiments[k].nstudents; l++) {
				 if(experiments[k].s[l]->id == j) { // found the student!
					 for(int m=l+1; m<experiments[k].nstudents; m++) { // move other students one step down
						 experiments[k].s[m-1] = experiments[k].s[m];
					 }
					 experiments[k].s[experiments[k].nstudents-1] = 0;
					 experiments[k].nstudents--;
				 }
			 }
		 }
	 }

	 if(talk) cout << "Student " << j << " is taking " << ntaking[j] << " experiments according to old schedule and should take a total of " << students[j].nexperiments << endl;

	 for(int i=0; i<students[j].nexperiments-extra_duration_student-ntaking[j]; i++){ // find an experiment for student to join
		 int texp;
     if(i<students[j].preferred.size())
		   texp = students[j].preferred[i];
		 else
		   texp = students[j].possible[i]; // could be a repeat, but we'll resolve that later

     // find the first experiment that has this
		 bool found_one = false;
		 int k;
		 for(k=0; k<nexperiments_total-extra_duration; k++){
			 if(experiments[k].time<frozen_time) continue; // do not add student to a past experiment

			 if(experiments[k].experiment_id==texp && experiments_duration[texp]<=students[j].nexperiments-extra_duration_student-ntaking[j]-i){
				 if(experiments[k].nstudents<3){ // heureka, there is a free spot!
					 experiments[k].s[experiments[k].nstudents] = &(students[j]);
					 experiments[k].nstudents++;
					 found_one = true;
					 if(talk) cout << "Student " << j << " assigned to preferred experiment slot " << k << endl;
				   break;
				 }
			 }
		 }
		 if(!found_one){ // try again, this time randomly assign
       for(k=0; k<nexperiments_total-extra_duration; k++){

			   if(experiments[k].time<frozen_time) continue; // do not add student to a past experiment

				 if(experiments[k].nstudents<3 && experiments_duration[experiments[k].experiment_id]<=students[j].nexperiments-extra_duration_student-ntaking[j]-i){ // heureka!
					 experiments[k].s[experiments[k].nstudents] = &(students[j]);
					 experiments[k].nstudents++;
					 if(talk) cout << "Student " << j << " assigned to random experiment slot " << k << endl;
					 found_one = true;
					 break; // we know we are going to find one since we've expanded the number of experiment slots offered to >= nexperiments_students/3
				 }
			 }

			 if(found_one == false) {
			   // if we reach here, the schedule is overfilled (possible when starting with an initial draw and reducing number of students later)
			   cerr << "The schedule is overfilled. This is possible when starting with an initial draw and reducing number of students later. You will have to manually edit the initial draw. Call Daniel." << endl;
         exit(1);
		   }
		 }
		 extra_duration_student += (experiments_duration[experiments[k].experiment_id]-1);
	 }
 }
}

// three simple helper function
int **newInt2d(int nfirst, int nsecond, int init=0) {

  int **a = new int*[nfirst];
  for (int i = 0; i<nfirst; i++)
    {
      a[i] = new int[nsecond];
      for (int j = 0; j<nsecond; j++) a[i][j] = init;
    }
  return a;
}

void deleteInt2d(int **a, int nfirst)
{
  for (int i = 0; i<nfirst; i++)
    {
      delete [] a[i];
    }
  delete [] a;
}


bool one_student_is_missing(student *fromlist[3], student *inlist[3], int nstudents)
{
  for (int i=0; i<nstudents; i++) {
    bool found = false;
    for (int j=0; j<nstudents; j++) {
      if (fromlist[i]->id==inlist[j]->id) found = true;
    }
    if (found==false) return true;
  }
  return false;
}

double rateDraw(vector<experiment> e, bool talk=false)
// evalute a given schedule according to all the differnt things that can be sub-optimal
{

  double r = 0.0; // "cost" of things that are sub-optimal in this schedule

  // six matrices to hold all relevant info for the rating
  int **advisor_date = newInt2d(nadvisors,nslots,0);
  int **advisor_experiment = newInt2d(nadvisors,nexperiments,0);
  int **student_experiment = newInt2d(nstudents,nexperiments,0);
  int **student_date = newInt2d(nstudents,nslots,0);
  int **student_student = newInt2d(nstudents,nstudents,0);
  int **experiment_date = newInt2d(nexperiments,nslots,0);
  int *date = new int[nslots]; for (int i=0; i<nslots; i++) date[i] = 0;


  // fill a bunch of matrices with integers so we can quickly get a penalty later
  for (int i=0; i<nexperiments_total-extra_duration; i++) {
    if (e[i].nstudents<1) {
      continue; // experiment is not actually happening without a student
    }
    if (e[i].nstudents==1) {
      if (talk) cout << "Experiment " << experiment_names[e[i].experiment_id] << " in slot " << e[i].time
                     << " is done by just one student, badness=" << student_nopartner << endl;
      r += student_nopartner;
    }
    if (e[i].nstudents==3) {
      if (talk) cout << "Experiment " << experiment_names[e[i].experiment_id] << " in slot " << e[i].time
                     << " is done by three students, badness=" << student_twopartners << endl;
      r += student_twopartners;
    }

    for (int t=e[i].time; t<e[i].time+experiments_duration[e[i].experiment_id]; t++) {
      date[t]++;
      advisor_date[e[i].a->id][t]++;
      experiment_date[e[i].experiment_id][t]++;
    }
    advisor_experiment[e[i].a->id][e[i].experiment_id]++;

    for (int j=0; j<e[i].nstudents; j++) {
      //cout << "student " << e[i].s[j]->id << " is doing experiment " << e[i].experiment_id << endl;
      student_experiment[e[i].s[j]->id][e[i].experiment_id]++;
      //cout << "student " << e[i].s[j]->id << " is doing that on date " << e[i].time << endl;
      for (int t=e[i].time; t<e[i].time+experiments_duration[e[i].experiment_id]; t++) {
        student_date[e[i].s[j]->id][t]++;
      }
      for (int k=0; k<e[i].nstudents; k++) {
        student_student[e[i].s[j]->id][e[i].s[k]->id]++;
      }
    }
  }

  // perspective of advisors: preferred/possible experiments, no blocked dates, no double advising, requested number of appointments
  for (int i=0; i<nadvisors; i++) {
    int n_different_experiments_advisor=0;

    for (int j=0; j<nexperiments; j++) {
      if (advisor_experiment[i][j]<1) // this advisor never teaches this experiment
        continue;

      n_different_experiments_advisor++;

      if (find(advisors[i].possible.begin(), advisors[i].possible.end(), j) == advisors[i].possible.end()) { // an impossible experiment
        if (talk) cout << "!!! Advisor " << advisors[i].name << " is doing an impossible experiment, "
                       << experiment_names[j] << ", badness=" << advisor_experiment[i][j]*advisor_impossible_experiment << endl;
        r += advisor_experiment[i][j]*advisor_impossible_experiment;
      }
      else if (find(advisors[i].preferred.begin(), advisors[i].preferred.end(), j) == advisors[i].preferred.end()) { // an unpreferred experiment
        if (talk) cout << "Advisor " << advisors[i].name << " is doing an unpreferred experiment, "
                       << experiment_names[j] << ", badness=" << advisor_experiment[i][j]*advisor_unpreferred_experiment << endl;
        r += advisor_experiment[i][j]*advisor_unpreferred_experiment;
      }
    }

    if (n_different_experiments_advisor>1) {
      if (talk) cout << "Advisor " << advisors[i].name << " is doing " << n_different_experiments_advisor
                     << " different experiments, badness=" << (n_different_experiments_advisor-1)*advisor_multipleexperiments << endl;
      r += (n_different_experiments_advisor-1)*advisor_multipleexperiments;
    }

    int nexperiments_advisor = 0;
    for (int j=0; j<nslots; j++) {
      nexperiments_advisor += advisor_date[i][j];
      if (advisor_date[i][j]>1) { // multiple bookings of same advisor on this date
        if (talk) cout << "!!! Advisor " << advisors[i].name << " is double booked in slot " << j
                       << ", badness=" << (advisor_date[i][j]-1)*advisor_double_booking << endl;
        r += (advisor_date[i][j]-1)*advisor_double_booking;
      }
    }
    //if (talk) cout << "advisor " << i << " r=" << r << endl;

    if (talk && (nexperiments_advisor-advisors[i].nexperiments))
      cout << "Advisor " << advisors[i].name << " is teaching wrong number of experiments, "
           << nexperiments_advisor << " instead of " << advisors[i].nexperiments
           << ", badness=" << advisor_wrongsws*pow(nexperiments_advisor-advisors[i].nexperiments,2) << endl;
    r += advisor_wrongsws*pow(nexperiments_advisor-advisors[i].nexperiments,2);

    for (vector<int>::iterator iter = advisors[i].notime.begin(); iter<advisors[i].notime.end(); iter++)
      {
        if (advisor_date[i][*iter]>0) {
          if (talk) cout << "!!! Advisor " << advisors[i].name << " is booked on wrong slot " << *iter
                         << ", badness=" << advisor_notime << endl;
          r += advisor_notime;
        }
      }

  }

  if (talk) cout << "badness after advisor perspective: " << r << endl;

  // perspective of students: preferred/possible experiments, no double experimenting, total number of lab partners is appropriate
  for (int i=0; i<nstudents; i++) {
    for (int j=0; j<nexperiments; j++) {
      if (student_experiment[i][j]<1) // this student never does this experiment
        continue;

      if (student_experiment[i][j]>1) {// student does this experiment multiple times
        if (talk) cout << "!!! Student " << students[i].name << " is doing experiment "
                       << experiment_names[j] << " " << student_experiment[i][j] << " times, badness="
                       << student_double_experiment*(student_experiment[i][j]-1) << endl;
        r += student_double_experiment*(student_experiment[i][j]-1);
      }

      if (find(students[i].possible.begin(), students[i].possible.end(), j) == students[i].possible.end()) { // an impossible experiment
        if (talk) cout << "!!! Student " << students[i].name << " is doing an impossible experiment, " << experiment_names[j]
                       << ", badness=" << student_experiment[i][j]*student_impossible_experiment << endl;
        r += student_experiment[i][j]*student_impossible_experiment;
      }
      else if (find(students[i].preferred.begin(), students[i].preferred.end(), j) == students[i].preferred.end()) { // an unpreferred experiment
        if (talk) cout << "Student " << students[i].name << " is doing an unpreferred experiment, "
                       << experiment_names[j] << ", badness=" << student_experiment[i][j]*student_unpreferred_experiment << endl;
        r += student_experiment[i][j]*student_unpreferred_experiment;
      }
    }
    for (int j=0; j<nslots; j++) {
      if (student_date[i][j]>1) {// multiple bookings of same student on this date
        if (talk) cout << "!!! Student " << students[i].name << " is double booked in slot " << j
                       << ", badness=" << (student_date[i][j]-1)*student_double_booking << endl;
        r += (student_date[i][j]-1)*student_double_booking;
      }
    }
    int npartners = 0;
    for (int j=0; j<nstudents; j++) {
      if (student_student[i][j]<1 || j==i)
        continue;

      npartners++;

      if (find(students[i].dislike.begin(), students[i].dislike.end(), j) != students[i].dislike.end()) { // found
        if (talk) cout << "Student " << students[i].name << " is working with student " << students[j].name
                       << ", which they don't like" << endl;
        r += student_disliked_partner*student_student[i][j];
      }
      if (find(students[i].like.begin(), students[i].like.end(), j) == students[i].like.end()) { // not found
        if (talk) cout << "Student " << students[i].name << " is working with student " << students[j].name
                       << ", which they didn't choose" << endl;
        r += student_random_partner*student_student[i][j];
      }
    }
    if (talk && (npartners != ideal_npartners))
      cout << "Student " << students[i].name << " has " << npartners << " rather than the ideal number of partners, badness="
           << student_wrong_npartner*pow(npartners - ideal_npartners,2) << endl;
    r += student_wrong_npartner*pow(npartners - ideal_npartners,2);
  }


  if (talk) cout << "badness after student perspective: " << r << endl;

  // perspective of slots: no more than nrooms experiments in any slot, no more simultaneous same experiments as allowed
  for (int i=0; i<nslots; i++) {
    if (date[i]>nrooms) {
      if (talk) cout << "!!! Rooms are overbooked in slot " << i << " with " << date[i]
                     << " required, badness=" << rooms_overflow*(date[i]-nrooms) << endl;
      r += rooms_overflow*(date[i]-nrooms);
    }
    for (int j=0; j<nexperiments; j++) {
      if (experiment_date[j][i]>experiments_simultaneous[j]) {
        if (talk) cout << "!!! Experiment " << experiment_names[j] << " is simultaneously happening "
                       << experiment_date[j][i] << " times in slot " << i << ", badness="
                       << experiments_overflow*(experiment_date[j][i]-experiments_simultaneous[j]) << endl;
        r += experiments_overflow*(experiment_date[j][i]-experiments_simultaneous[j]);
      }

      if (experiment_date[j][i]>0 && experiments_earliestslot[j]>i) {
        if (talk) cout << "!!! Experiment " << experiment_names[j] << " is happening too early in slot " << i
                       << ", badness = " << experiment_tooearly << endl;
        r += experiment_tooearly;
      }
    }
  }


  if (talk) cout << "badness after room perspective: " << r << endl;

  // cleanup
  deleteInt2d(advisor_date,nadvisors);
  deleteInt2d(advisor_experiment,nadvisors);
  deleteInt2d(student_experiment,nstudents);
  deleteInt2d(student_date,nstudents);
  deleteInt2d(student_student,nstudents);
  deleteInt2d(experiment_date,nexperiments);
  delete [] date;

  return r;
}


void printDraw(vector<experiment> e, bool nice=false)
// writes out schedule; could be prettier
{
  //cout << "============= RAW LIST =============" << endl;
  //cout << "#ID DATE EXPERIMENT_ID ADVISOR>0?" << endl;
  //for (int i=0; i<nexperiments_total; i++) {
  //         if (e[i].a==0) cout << i << " " << e[i].time << " " << e[i].experiment_id << " NULL" << endl;
  //         else cout << i << " " << e[i].time << " " << e[i].experiment_id << " OK " << e[i].a->id << endl;
  //}
  cout << "============= SCHEDULE =============" << endl;
  cout << "#ID DATE EXPERIMENT ADVISOR STUDENTS" << endl;
  for (int d=0; d<nslots; d++) {
    for (int i=0; i<nexperiments_total-extra_duration; i++) {
      if (e[i].nstudents<1) continue;
      if (e[i].time!=d) continue;
      cout << i << " " << e[i].time << " " << experiment_names[e[i].experiment_id] << " " << e[i].a->name;
      for (int j=0; j<e[i].nstudents; j++) {
        cout << " " << e[i].s[j]->name;
      }
      cout << endl;
    }
  }
  cout << endl;


  if (nice==false) {
    cout << "====================================" << endl;
    return;
  }


  cout << "========== ADVISOR PERSPECTIVE ==========" << endl;
  for (int j=0; j<nadvisors; j++) {
    cout << "Schedule for advisor " << advisors[j].name << ":" << endl;
    for (int d=0; d<nslots; d++) {
      for (int i=0; i<nexperiments_total-extra_duration; i++) {
        if (e[i].nstudents<1) continue;
        if (d < e[i].time || d >= e[i].time+experiments_duration[e[i].experiment_id]) continue;
        if (e[i].a->id != j) continue;
        cout << e[i].time+1 << " " << experiment_names[e[i].experiment_id] << " " << e[i].a->name;
        for (int k=0; k<e[i].nstudents; k++) {
          cout << " " << e[i].s[k]->name;
        }
        cout << endl;
      }
    }
    cout << endl;
  }
  cout << endl;

  cout << "========== DATE PERSPECTIVE ==========" << endl;
  for (int j=0; j<nslots; j++) {
    cout << "Schedule for slot " << j << ":" << endl;
    for (int i=0; i<nexperiments_total-extra_duration; i++) {
      if (e[i].nstudents<1)
        continue;
      if (j < e[i].time || j >= e[i].time+experiments_duration[e[i].experiment_id])
        continue;
      cout << e[i].time+1 << " " << experiment_names[e[i].experiment_id] << " " << e[i].a->name;
      for (int k=0; k<e[i].nstudents; k++) {
        cout << " "<< e[i].s[k]->name;
      }
      cout << endl;
    }
    cout << endl;
  }
  cout << endl;

  cout << "========== EXPERIMENT PERSPECTIVE ==========" << endl;
  for (int j=0; j<nexperiments; j++) {
    cout << "Schedule for experiment " << experiment_names[j] << ":" << endl;
    for (int d=0; d<nslots; d++) {
      for (int i=0; i<nexperiments_total-extra_duration; i++) {
        if (e[i].nstudents<1)
          continue;
        if (d < e[i].time || d >= e[i].time+experiments_duration[e[i].experiment_id])
          continue;
        if (e[i].experiment_id != j)
          continue;
        cout << e[i].time+1 << " " << experiment_names[e[i].experiment_id] << " " << e[i].a->name;
        for (int k=0; k<e[i].nstudents; k++) {
          cout << " " << e[i].s[k]->name;
        }
        cout << endl;
      }
    }
    cout << endl;
  }
  cout << endl;

  cout << "========== STUDENT PERSPECTIVE ==========" << endl;
  for (int j=0; j<nstudents; j++) {
    cout << "Schedule for student " << students[j].name << ":" << endl;
    for (int d=0; d<nslots; d++) {
      for (int i=0; i<nexperiments_total-extra_duration; i++) {
        if (e[i].nstudents<1)
          continue;
        if (d < e[i].time || d >= e[i].time+experiments_duration[e[i].experiment_id])
          continue;
        bool found = false;
        for (int k=0; k<e[i].nstudents; k++) {
          if (e[i].s[k]->id == j) found = true;
        }
        if (found == false)
          continue;

        cout << " " << e[i].time+1 << " " << experiment_names[e[i].experiment_id] << " " << e[i].a->name;
        for (int k=0; k<e[i].nstudents; k++) {
          cout << " " << e[i].s[k]->name;
        }
        cout << endl;
      }
    }

    cout << "====================================" << endl;
  }
}




int main(int atoi, char **argv)
{
	if(atoi < 4 || atoi > 7)
	{
	 cout << "syntax: " << argv[0] << " [experiments file] [advisor file] [student file] ([filename to write schedule to] [filename to read initial schedule from] [last slot to keep as is (starting at 1)])" << endl;
	 return 1;
	}

	double t = thot;

  cout << "integer as random seed = ";
  int rseed;
  cin >> rseed;
  cout << endl;
  srand(rseed);

  int frozen_time = 0;
	if(atoi==7) frozen_time = stoi(argv[6]);

  if(atoi<6) {
	  readFiles(argv[1],argv[2],argv[3]);
  } else {
    t = tslowcoolmax;
		readFiles(argv[1],argv[2],argv[3],argv[5],frozen_time);
	}
	cout << "All parameters were entered successfully.\nPlease wait while astrolanneal is optimizing the lab schedule." << endl;

  int iterations = 0;
  unsigned int it = 0;


  cout << "======================= Initial schedule =======================" << endl;
  //cout << "size of experiment list " << experiments.size() << " should be " << nexperiments_total-extra_duration << endl;
  printDraw(experiments, true);
  double oldrating = rateDraw(experiments,false);
  double bestrating = oldrating;
  vector<experiment> best = experiments;
  cout << "================================================================" << endl;

  while (iterations<freeze_timeout) {

    //cout << "================ iteration " << iterations << endl;
    //printDraw(experiments, true);

    iterations++;

    vector<experiment> working = experiments; // working copy

    //cout << "printing times" << endl;
		//for(int i=0; i<(nexperiments_total-extra_duration); i++)
		//  cout << i << " " << working[i].time << endl;


		int nrandchat, nrandchat2;
		do {
		    nrandchat  = rand()%(nexperiments_total-extra_duration);
				nrandchat2 = rand()%(nexperiments_total-extra_duration);
				//cout << nrandchat << " " << nrandchat2 << " " << working[nrandchat].time << " " << working[nrandchat2].time << " " << frozen_time << endl;
		    //if(nrandchat==0) return 0;
		} while (working[nrandchat].time < frozen_time || working[nrandchat2].time < frozen_time || nrandchat == nrandchat2);

		int bufint;
		int i=0;

    int tmptime, tmpexperiment, tmps1, tmps2;
    student *tmpstudent;
    advisor *tmpa;

    int ra = rand()%8; // none of the multi-slot changes for now

    switch(ra) {
    case 0: // change time slot of an existing experiment randomly
		  {
		  int newtime = rand()%(nslots+1-experiments_duration[working[nrandchat].experiment_id]);
			if(newtime >= frozen_time)
      	working[nrandchat].time = newtime; // never go beyond schedule
			}
      break;

    case 1: // swap two experiments in time
      tmptime = working[nrandchat].time;
      working[nrandchat].time = working[nrandchat2].time;
      working[nrandchat2].time = tmptime;
      // now ensure we do not go beyond schedule
      if ((working[nrandchat].time>(nslots-experiments_duration[working[nrandchat].experiment_id])) && (nslots-experiments_duration[working[nrandchat].experiment_id])>=frozen_time)
        working[nrandchat].time=(nslots-experiments_duration[working[nrandchat].experiment_id]);
      if ((working[nrandchat2].time>(nslots-experiments_duration[working[nrandchat2].experiment_id])) && (nslots-experiments_duration[working[nrandchat2].experiment_id])>=frozen_time)
        working[nrandchat2].time=(nslots-experiments_duration[working[nrandchat2].experiment_id]);
      break;

    case 2: // change experiment of an existing experiment
		  {
	      int re;
	      do {
	        re = (rand()%nexperiments);
	      } while (experiments_duration[re] != experiments_duration[working[nrandchat].experiment_id]); // never swap experiments of different duration; can't do that and keep student's numbers of experiments correct
	      working[nrandchat].experiment_id = re;
		  }
      break;

    case 3: // swap experiments
      while (experiments_duration[working[nrandchat].experiment_id] != experiments_duration[working[nrandchat2].experiment_id] || working[nrandchat2].time < frozen_time) {
        // never swap experiments of different duration; can't do that and keep student's numbers of experiments correct
        nrandchat2 = rand()%(nexperiments_total-extra_duration);
      }
      // nrandchat may be equal to nrandchat2 but that does no harm
      tmpexperiment = working[nrandchat].experiment_id;
      working[nrandchat].experiment_id = working[nrandchat2].experiment_id;
      working[nrandchat2].experiment_id = tmpexperiment;
      break;

    case 4: // move student to a different experiment
      while (experiments_duration[working[nrandchat].experiment_id] != experiments_duration[working[nrandchat2].experiment_id] || working[nrandchat2].time < frozen_time) {
        // never swap experiments of different duration; can't do that and keep student's numbers of experiments correct
        nrandchat2 = rand()%(nexperiments_total-extra_duration);
      }

      if (nrandchat!=nrandchat2 && working[nrandchat].nstudents>0 && working[nrandchat2].nstudents<3) {
        //cout << "moving student from " << nrandchat << " to " << nrandchat2 << endl;
        working[nrandchat2].s[working[nrandchat2].nstudents] = working[nrandchat].s[working[nrandchat].nstudents-1];
        working[nrandchat].s[working[nrandchat].nstudents-1]=0;
        working[nrandchat].nstudents--;
        working[nrandchat2].nstudents++;
      }
      break;

    case 5: // swap students between experiments - with some caution, this we can do while keeping their total number of time spent conserved
      if (working[nrandchat].nstudents>0 && working[nrandchat2].nstudents>0 &&
          experiments_duration[working[nrandchat].experiment_id] == experiments_duration[working[nrandchat2].experiment_id]) {
        tmps1 = rand()%working[nrandchat].nstudents;
        tmps2 = rand()%working[nrandchat2].nstudents;
        tmpstudent = working[nrandchat2].s[tmps2];
        working[nrandchat2].s[tmps2] = working[nrandchat].s[tmps1];
        working[nrandchat].s[tmps1]=tmpstudent;

        /* // might be buggy? not sure
        // repair potential damage to net time spent by each student
        for (int d=0; d<experiments_duration[working[nrandchat].experiment_id]-experiments_duration[working[nrandchat2].experiment_id]; d++) {
        // working[nrandchat] takes longer than working[nrandchat2]; delete an experiment for student working[nrandchat].s[tmps1], add an experiment for student working[nrandchat2].s[tmps2]

        // delete student from a one-slot experiment
        int k=0;
        int de,ds;
        do {
        do {
        de = rand()%(nexperiments_total-extra_duration); // random experiment
        } while(working[de].nstudents<1); // need an experiment where there is at least one active student
        ds = rand()%working[de].nstudents;
        k++;
        if (k>1000) break; // break out of this do loop
        }        while(working[de].nstudents==0 || experiments_duration[working[de].experiment_id]>1 || working[de].s[ds]->id != working[nrandchat].s[tmps1]->id); // a potential infinite loop for students that only have one two-slot experiment

        if (k>1000) break; // break out of this switch

        working[de].nstudents--;
        working[de].s[ds] = working[de].s[working[de].nstudents];

        // add other student to a one-slot experiment
        do {
        de = rand()%(nexperiments_total-extra_duration); // random experiment
        } while(working[de].nstudents>=3); // do not overcrowd experiment
        working[de].s[working[de].nstudents] = working[nrandchat2].s[tmps2];
        working[de].nstudents++;
        }
        */

      }
      break;

    case 6: // change advisor
      working[nrandchat].a = &(advisors[rand()%nadvisors]);
      break;

    case 7: // swap advisors

      tmpa = working[nrandchat].a;
      working[nrandchat].a = working[nrandchat2].a;
      working[nrandchat2].a = tmpa;
      break;

    case 8: // swap a N slot experiment for a sequence of experiments adding up to N, done by the same students subsequently
      {
        if (experiments_duration[working[nrandchat].experiment_id]<2) break; // do nothing if chosen experiment is not a multi-slot experiment

        // we are planning to create new experiments in working[] and then delete working[nrandchat]
        int nnew = 0;

        // potentially create new experiments if we need more than advisors say they want to advise
        do
          {
            int i = rand() % nadvisors;
            // creating an additional experiment for advisor i

            experiment e;
            e.a = &(advisors[i]);
            e.s[0] = working[nrandchat].s[0]; e.s[1] = working[nrandchat].s[1]; e.s[2] = working[nrandchat].s[2];
            e.nstudents = working[nrandchat].nstudents;
            e.time = rand()%(nslots-max_duration+1); // never exceeds timeframe this way

            if (advisors[i].preferred.size()>0)
              e.experiment_id = advisors[i].preferred[rand()%advisors[i].preferred.size()];
            else
              e.experiment_id = advisors[i].possible[rand()%advisors[i].possible.size()];

            if (experiments_duration[e.experiment_id]>experiments_duration[working[nrandchat].experiment_id]-nnew) {
              // doesn't fit, let's try again; caveat of endless loop
            } else {
              experiments.push_back(e);
              extra_duration += (experiments_duration[e.experiment_id]-1);
              nnew += experiments_duration[e.experiment_id];
            }

          } while (nnew < experiments_duration[working[nrandchat].experiment_id]);

        extra_duration -= (experiments_duration[working[nrandchat].experiment_id]-1); // TODO: only do this if actually accepted ...
        working.erase(working.begin() + nrandchat);
      }
      break;

    case 9: // swap a set of 2 experiments adding up to N, done by the same students, for an N slot experiment

      // find set of experiments these students are sharing
      int nstudents = working[nrandchat].nstudents;
      vector<int> group_experiments;
      for (int i=0; i<nexperiments_total-extra_duration; i++) {
        if (i==nrandchat)
          continue;
        if (experiments[i].nstudents != working[nrandchat].nstudents)
          continue;
        if (one_student_is_missing(working[nrandchat].s, experiments[i].s, working[nrandchat].nstudents))
          continue;

        group_experiments.push_back(i);
      }
      if (group_experiments.size()<1)
        break; // no other experiment found

      // find all experiments that take more slots than nrandchat

      vector<int> big_experiments;
      for (int i=0; i<experiments_duration.size(); i++) {
        // TODO: continue here

      }

    }

    //cout << "rating" << endl;
    double workingrating = rateDraw(working);
    double delta = oldrating - workingrating; // if this is positive then working is better
    //cout << delta << " saving" << endl;
    if (delta>0) {
      experiments = working;
      oldrating = workingrating;
      iterations = 0;
      if (oldrating<bestrating) {
        bestrating = oldrating;
        best = experiments;
      }
    }
    if (delta<0) {
      double x=double(rand()%RAND_MAX)/double(RAND_MAX);
      if (x<exp(delta/t)) { // Boltzmann
        //if (rand()%10==0)
        //cout << "accepted something worse with p=" << x << endl;
        experiments = working;
        oldrating = workingrating;
        iterations = 0;
      }
    }
    if (delta==0) {
      if (rand()%2) {
        experiments = working;
        oldrating = workingrating;
        //cout << "~";
      }
    }

    if (t<tslowcoolmax && t>tslowcoolmin) {
      t *= 1.-(1.-tfactor)/tslowdownfactor;
    } else {
      t *= tfactor;
    }
    it++;

    if (it%100000==0) {
      cout << "status after " << it << " iterations: temperature " << t << "K, ";
      cout << oldrating << " badness" << endl;
    }
  }

  cout << "\n\tYour schedule has been optimized successfully.\nstatus:\n";
  float g = rateDraw(best, true);
  cout << "\nbadness of " << g << "\n\n";

  printDraw(best, true);

  if(atoi>4) {
    ofstream out;
		out.open(argv[4]);
		out << "#Advisor_ID__Student1_ID__Student2_ID__Student3_ID__NStudents__Timeslot__Experiment_ID__Experiment_Name__Advisor_Name__Student_Names" << endl;
		for(int i=0; i<nexperiments_total-extra_duration; i++) {
      out << experiments[i].a->id << " ";
			for(int j=0; j<experiments[i].nstudents; j++) {
         out << experiments[i].s[j]->id << " ";
      }
			for(int j=experiments[i].nstudents; j<3; j++) {
				 out << "-1 ";
			}
			out << experiments[i].nstudents << " " << experiments[i].time << " " << experiments[i].experiment_id << " ";

			out << experiment_names[experiments[i].experiment_id] << " " << experiments[i].a->name << " ";
      for(int j=0; j<experiments[i].nstudents; j++) {
				out << experiments[i].s[j]->name << " ";
			}
			for(int j=experiments[i].nstudents; j<3; j++) {
				 out << "n/a ";
			}
			out << endl;
	  }
	}

	return 0;
}
